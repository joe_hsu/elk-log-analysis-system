# ELK-log-analysis-system

## RUN
```
chmod +x install.sh
sudo ./install.sh
```

## Config
### elasticsearch
#### How can I persist Elasticsearch data?
```
elasticsearch:
  volumes:
    - /path/to/storage:/usr/share/elasticsearch/data
```

### logstatsh
#### filter pattern
modify filter patterns:
logstash/pipeline/logstash.conf 

#### port listener
current logstash listen port list
5000 -> beats

5001 -> syslog 

5002 -> beats

5003 -> tcp

if you want to modify the port, change logstash.conf and docker-compose.yml files.

## debug ELK log info
```
cd elk-log-analysis-system
docker-compose logs -f
```

## restart service
```
docker-compose down
docker-compose up -d
```

## reference
https://github.com/deviantony/docker-elk#how-can-i-persist-elasticsearch-data