input {
	beats {
		port => 5000
	}

  syslog {
    port => 5001
  }

  beats {
    port => 5002
  }

  tcp {
	   port => 5003
     codec => json_lines
  }
}

## Add your filters / logstash plugins configuration here
filter {

  if [fields][log_type]=="system_log" {
      grok {
          break_on_match => true
          match => {
             "message" => [
               "^\[%{GREEDYDATA:plugin}\] (?<timestamp>[0-9]*\/+[0-9]*\/+[0-9]* - [0-9]+:[0-9]+:[0-9]+) \| %{NUMBER:status:integer} \| %{GREEDYDATA:duration} \|%{GREEDYDATA}%{IP:client} \|%{GREEDYDATA}%{GREEDYDATA:API}$",
               "^time=\"%{TIMESTAMP_ISO8601:timestamp}\" level=%{GREEDYDATA:debug_level} msg=\"(?<API>\[+[a-zA-Z0-9]*\]+)\[%{GREEDYDATA:trace_id}\](?<API_ID>\[+[a-zA-Z0-9]*\]+) %{GREEDYDATA:server_message}\" Module=%{GREEDYDATA:server_module} service_name=%{GREEDYDATA:service_name}$",
               "^time=\"%{TIMESTAMP_ISO8601:timestamp}\" level=%{GREEDYDATA:debug_level} msg=\"(?<API>\[+[a-zA-Z0-9]*\]+) %{GREEDYDATA:server_message}\" Module=%{GREEDYDATA:server_module}$"
             ]
          }
      }

      date {
          match =>["timestamp","yyyy-MM-dd'T'HH:mm:ssZ","yyyy/MM/dd - HH:mm:ss"]
          target =>"@timestamp"
          "locale"=>"en"
      }

      if [API] and [API_ID] {
          mutate {
              merge => {"API"=>"API_ID"}
              remove_field => ["API_ID"]
          }
      }
  }

  ## 192.168.1.152 is local rockme frondend nginx server
  ## 54.95.147.3 is AWS rockme frondend nginx server
  if [host]=="192.168.1.152" or [host]=="54.95.147.3" {
      grok {
          match => { "message" => "%{COMBINEDAPACHELOG}+%{GREEDYDATA:extra_fields}"}
          overwrite => [ "message" ]

      }

      grok {
          match => { "extra_fields" => "\"%{IPORHOST:clientip}\""}
          overwrite => ["clientip"]
      }

      mutate {
          convert => {
              "response" => "integer"
               "bytes" => "integer"
               "responsetime" => "float"
           }
      }

      geoip {
          source => "clientip"
          target => "geoip"
          add_tag => ["nginx-geoip"]
      }

      useragent {
          source => "agent"
      }

      if [name] == "okhttp" or [device] == "Generic Smartphone" {
          mutate {update =>{"device" => "Android"} }
      } else if [os] == "iOS" {
          mutate {update =>{"device" => "iPhone"} }
      }
  }

  ## if data contains service field, parse log pattern
  ## this match pattern for tidedog now
  if [service] {
      mutate {
          lowercase => [ "service" ]
      }

      grok {
          match => {"message"=> "^(?<API>\[+[a-zA-Z0-9]*\]+)%{GREEDYDATA}%{GREEDYDATA:server_message}$"}
          match => {"server_message"=> "%{URI:URL}"}
          match => {"server_message"=> "(?<wallet_address>0x[0-9A-Za-z]+)"}
      }

      date {
          match =>["timestamp","yyyy-MM-dd'T'HH:mm:ssZ","yyyy/MM/dd - HH:mm:ss"]
          target =>"@timestamp"
          "locale"=>"en"
      }
  }
}

output {
  ## if data contains service field, change ES index name
  if [service] {
      elasticsearch {
          hosts => "elasticsearch:9200"
          index => "%{service}-%{+YYYY.MM.dd}"
      }
  } else {
      elasticsearch {
          hosts => "elasticsearch:9200"
      }
  }
}

