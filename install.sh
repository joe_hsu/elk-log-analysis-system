#!/bin/bash
apt-get update
apt-get install docker docker-compose
mkdir ./elasticsearch/data
chmod 777 ./elasticsearch/data
docker-compose up -d
